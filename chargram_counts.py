#!/usr/bin/env python3

"""Given a text return all the available chargram token stats.

Suppose we are given "Hello World". This script will return all the possible
chargram counts while respecting the exisiting segmentation. For example,
    Hello -> [('H', 1), ('e', 1), ('l', 2), ('o', 1), ('He', 1), ('el', 1),
              ('ll', 1), ('lo', 1), ('Hel', 1), ('ell', 1), ('llo', 1),
              ('Hell', 1), ('ello', 1), ('Hello', 1)]
"""

__author__ = 'Hwasung Lee'


import multiprocessing
import re
import sys


def ChargramPerWord(word):
  """Given a word, return the chargrams as a list."""
  def ChargramInternal(chargrams, word, arity):
    """Internal implmentation of finding chargrams."""
    if len(word) < arity:
      return chargrams
    else:
      try:
        chargrams[word[:arity]] += 1
      except KeyError:
        chargrams[word[:arity]] = 1
      return ChargramInternal(chargrams, word[1:], arity)

  chargrams = {}
  for arity in range(1, len(word)+1):
    chargrams = ChargramInternal(chargrams, word, arity)

  return [(k, v) for k, v in chargrams.items()]


def NextWordsInLines(n, iterable):
  """Return a list of words in the next lines of iterable."""
  ignore_punct = r'[,.!?]$'
  results = []
  for _ in range(n):
    try:
      results += [re.sub(ignore_punct, '', w) for w in next(iterable).split()]
    except StopIteration:
      break

  return results


def main():
  """main function which runs when the script is called standalone."""
  workers = multiprocessing.Pool(multiprocessing.cpu_count())
  batch_size = 10000
  stdin_striped = map(str.strip, sys.stdin)
  sys.stderr.write('Counting chargrams with batch size: %s\n' % batch_size)

  wordlist = NextWordsInLines(batch_size, stdin_striped)
  counter = 0
  results = {}
  while wordlist:
    for l in workers.map(ChargramPerWord, wordlist):
      for k, v in l:
        try:
          results[k] += v
        except KeyError:
          results[k] = v

    wordlist = NextWordsInLines(batch_size, stdin_striped)
    counter += 1
    sys.stderr.write('Lines processed: %s\n' % str(counter*batch_size))

  sorted_results = sorted(results.items(), key=lambda x: x[1], reverse=True)
  for k, v in sorted_results:
    if len(k) > 1:
      sys.stdout.write('%s\t%s\n' % (k, v))


if __name__ == '__main__':
  main()

