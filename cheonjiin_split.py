#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Convert decoomposed Hangeul sequence to Cheonjiin."""

__author__ = 'Hwasung Lee'


import sys


def CheonjiinDecompose(line):
  """Decomposing Jamo into Cheonjiin."""
  cheonjiin_table = {
      'ㅏ': 'ㅣ\u318D',
      'ㅐ': 'ㅣ\u318Dㅣ',
      'ㅑ': 'ㅣ\u318D\u318D',
      'ㅒ': 'ㅣ\u318D\u318Dㅣ',

      'ㅓ': '\u318Dㅣ',
      'ㅔ': '\u318Dㅣㅣ',
      'ㅕ': '\u318D\u318Dㅣ',
      'ㅖ': '\u318D\u318Dㅣㅣ',

      'ㅗ': '\u318Dㅡ',
      'ㅘ': '\u318Dㅡㅣ\u318D',
      'ㅙ': '\u318Dㅡㅣ\u318Dㅣ',
      'ㅚ': '\u318Dㅡㅣ',
      'ㅛ': '\u318D\u318Dㅡ',

      'ㅜ': 'ㅡ\u318D',
      'ㅝ': 'ㅡ\u318D\u318Dㅣ',
      'ㅞ': 'ㅡ\u318D\u318Dㅣㅣ',
      'ㅟ': 'ㅡ\u318Dㅣ',
      'ㅠ': 'ㅡ\u318D\u318D',

      'ㅢ': 'ㅡㅣ',
  }

  result = []
  for c in line:
    if c in cheonjiin_table:
      result.append(cheonjiin_table[c])
    else:
      result.append(c)

  return ''.join(result)


def main():
  """main function which runs when the script is called standalone."""
  for line in sys.stdin:
    sys.stdout.write(CheonjiinDecompose(line))


if __name__ == '__main__':
  main()

