#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Extracts UTF8 text out of STDIN."""

__author__ = 'Hwasung Lee'


import sys


def main():
  """Extracts utf8 strings from stdin."""
  # pylint: disable-msg=E1101,E1103
  _stdin = sys.stdin.detach()
  # pylint: enable-msg=E1101,E1103
  for line in _stdin:
    try:
      line = line.decode('utf8')
      sys.stdout.write(line)
    except UnicodeDecodeError:
      pass


if __name__ == '__main__':
  main()

