#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Compose hangeul."""

__author__ = 'Hwasung Lee'


import codecs
import unicodedata
import sys


_STDIN = codecs.getreader('utf8')(sys.stdin)
_STDOUT = codecs.getwriter('utf8')(sys.stdout)


def main():
  """main function which runs when the script is called standalone."""
  for line in _STDIN:
    _STDOUT.write(unicodedata.normalize('NFC', line))


if __name__ == '__main__':
  main()

