#!/usr/bin/env python3.2
# -*- coding: utf8 -*-


"""Hangeul extract tool.

Reads a line from STDIN and extract Hangeul (NFC), and Jamo (NFD and
Compatibility).
"""

__author__ = 'Hwasung Lee'


import argparse
import sys


def main():
  """Read in data from _STDIN and write out to _STDOUT."""
  parser = argparse.ArgumentParser(
      description='Decide which characters to keep')
  parser.add_argument('-b', '--block',
      dest='block',
      default='all',
      help='Which unicode block to keep.')
  args = parser.parse_args()

  hangeul_utils = HangeulUtils()

  for line in sys.stdin:
    line = line.strip()
    if args.block == 'all':
      line = hangeul_utils.FilterNFxAndCompatibility(line)
    if args.block.lower() == 'nfc':
      line = hangeul_utils.FilterNFC(line)

    if line:
      sys.stdout.write(line + '\n')


class HangeulUtils(object):
  """Class that holds Hangeul utils."""

  _allowed_whitespaces = [' ', '\t', '\n']

  def FilterNFC(self, line):
    """Passes Hangeul NFC and whitespaces only."""
    filtered_text = []
    for c in line:
      if self.__IsCharHangeulNFC(c) or c in self._allowed_whitespaces:
        filtered_text.append(c)

    return ''.join(filtered_text)

  def FilterNFxAndCompatibility(self, line):
    """Passes Hangeul NFC, NFD, and Compatiblity.

    We also allow whitespaces to be passed.
    """
    filtered_text = []
    for c in line:
      if self.__IsCharHangeulNFxOrCompatibility(c) or (
          c in self._allowed_whitespaces):
        filtered_text.append(c)

    return ''.join(filtered_text)

  def __IsCharHangeulNFxOrCompatibility(self, c):
    """Returns True/False if char is Hangeul NFx or Compatibility."""
    if (self.__IsCharHangeulNFC(c) or
        self.__IsCharHangeulNFD(c) or
        self.__IsCharHangeulCompatibility(c)):
      return True
    else:
      return False

  @staticmethod
  def __IsCharHangeulCompatibility(c):
    """Unicode Hangeul Compatibility block."""
    #   http://www.unicode.org/charts/PDF/U3130.pdf
    return 0x3131 <= ord(c) <= 0x318E

  @staticmethod
  def __IsCharHangeulNFC(c):
    """Unicode Hangeul NFC block."""
    #   http://www.unicode.org/charts/PDF/UAC00.pdf
    return 0xAC00 <= ord(c) <= 0xD7AF

  @staticmethod
  def __IsCharHangeulNFD(c):
    """Unicode Hangeul NFD block."""
    #   http://www.unicode.org/charts/PDF/U1100.pdf
    return 0x1100 <= ord(c) <= 0x11FF


if __name__ == '__main__':
  main()
