#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Extract Hangeul sentences by the proportion of Hangeul."""

__author__ = 'Hwasung Lee'


import argparse
import sys

from hangeul_unicode_lib import IsCharHangeul


def HangeulFraction(line):
  """Returns the fraction of Hangeul/Jamo of the given line."""
  count = 0
  for c in line:
    if IsCharHangeul(c):
      count += 1

  return count/len(line)


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(
      description='Filter out lines with Hangeul ratio.')
  parser.add_argument('-r', '--ratio',
      dest='ratio',
      type=float,
      default=0.7,
      help='The ratio of Hangeul we require.')
  args = parser.parse_args()

  for line in sys.stdin:
    if HangeulFraction(line) > args.ratio:
      sys.stdout.write(line)


if __name__ == '__main__':
  main()

