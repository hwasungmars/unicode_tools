#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Remove corrupted Hangeul lines.

We take a conservative approach and filter out known Hangeul characters that are
known to be results of corruption.

TODO(hwasung): Review whether a more aggressive approach is desirable.
"""

__author__ = 'Hwasung Lee'


import codecs
import sys


_STDIN = codecs.getreader('utf8')(sys.stdin)
_STDOUT = codecs.getwriter('utf8')(sys.stdout)
_STDERR = codecs.getwriter('utf8')(sys.stderr)


def main():
  """main function which runs when the script is called standalone."""

  def IsCorruptedLine(line):
    """Helper method that identifies corrupted lines."""
    corrupted_hangeul_char = [u'븮', u'뵵', u'뷫', u'켱', u'츮', u'뱳']
    for c in line:
      if c in corrupted_hangeul_char:
        return True

    return False

  for line in _STDIN:
    if not IsCorruptedLine(line):
      _STDOUT.write(line)
    else:
      _STDERR.write('WARN] Following line is corrupted: %s' % line)

if __name__ == '__main__':
  main()

