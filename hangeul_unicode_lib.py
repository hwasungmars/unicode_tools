#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Provides functions that maps one Hangeul Unicode block to another."""

__author__ = 'Hwasung Lee'


import unicodedata


def IsCharHangeulCompatibility(c):
  """Unicode Hangeul Compatibility block."""
  # http://www.unicode.org/charts/PDF/U3130.pdf
  return 0x3131 <= ord(c) <= 0x318E


def IsCharHangeulNFC(char):
  """Unicode Hangeul NFC block."""
  # http://www.unicode.org/charts/PDF/UAC00.pdf
  return 0xAC00 <= ord(char) <= 0xD7A3


def IsCharHangeulNFD(c):
  """Unicode Hangeul NFD block."""
  # http://www.unicode.org/charts/PDF/U1100.pdf
  return 0x1100 <= ord(c) <= 0x11FF


def IsCharHangeul(c):
  """Returns true false if a char is Hangeul/Jamo."""
  return (IsCharHangeulNFC(c) or
          IsCharHangeulNFD(c) or
          IsCharHangeulCompatibility(c))


def DoesNFCWordHaveCoda(word):
  """Checks whether a NFC hangeul char has coda."""
  last_char = word[-1]
  return IsCharHangeulNFC(last_char) and (ord(last_char)-0xAC00)%28 != 0


def HangeulCompatibilityToNFDCoda(line):
  """Converts all compatiblity Hangeul to NFD coda."""
  # pylint: disable-msg=W1402
  compatibility_to_nfd_coda = {
      '\u3131' : '\u11A8',  # ㄱ
      '\u3132' : '\u11A9',  # ㄲ
      '\u3133' : '\u11AA',  # ㄱㅅ
      '\u3134' : '\u11AB',  # ㄴ
      '\u3135' : '\u11AC',  # ㄴㅈ
      '\u3136' : '\u11AD',  # ㄴㅎ
      '\u3137' : '\u11AE',  # ㄷ
      '\u3139' : '\u11AF',  # ㄹ
      '\u313A' : '\u11B0',  # ㄹㄱ
      '\u313B' : '\u11B1',  # ㄹㅁ
      '\u313C' : '\u11B2',  # ㄹㅂ
      '\u313D' : '\u11B3',  # ㄹㅅ
      '\u313E' : '\u11B4',  # ㄹㅌ
      '\u313F' : '\u11B5',  # ㄹㅍ
      '\u3140' : '\u11B6',  # ㄹㅎ
      '\u3141' : '\u11B7',  # ㅁ
      '\u3142' : '\u11B8',  # ㅂ
      '\u3144' : '\u11B9',  # ㅂㅅ
      '\u3145' : '\u11BA',  # ㅅ
      '\u3146' : '\u11BB',  # ㅆ
      '\u3147' : '\u11BC',  # ㅇ
      '\u3148' : '\u11BD',  # ㅈ
      '\u314A' : '\u11BE',  # ㅊ
      '\u314B' : '\u11BF',  # ㅋ
      '\u314C' : '\u11C0',  # ㅌ
      '\u314D' : '\u11C1',  # ㅍ
      '\u314E' : '\u11C2',  # ㅎ
  }
  # pylint: enable-msg=W1402

  conversion_result = []
  for c in line:
    if 0x3131 <= ord(c) <= 0x314E:
      c = compatibility_to_nfd_coda[c]

    conversion_result.append(c)

  return ''.join(conversion_result)


def NormalizeToCodaAndNFD(line):
  """Map Hangeul compatibility to NFD coda and NFC to NFD."""
  line = HangeulCompatibilityToNFDCoda(line)
  return unicodedata.normalize('NFD', line)


