#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Remove Korean suffix for a given word."""

__author__ = 'Hwasung Lee'


import re
import unicodedata
import codecs
import sys
import os
from optparse import OptionParser
import multiprocessing

import hangeul_unicode_lib


_STDIN = codecs.getreader('utf8')(sys.stdin)
_STDOUT = codecs.getwriter('utf8')(sys.stdout)


def main():
  """Main function for the suffix util."""
  parser = OptionParser()
  parser.add_option('-f', '--suffix_file',
      dest='suffix_file',
      default=
          os.path.dirname(os.path.realpath(__file__))+'/korean_suffix_list.txt',
      help='Suffix file to load.')
  parser.add_option('-o', '--operation',
      dest='operation',
      help='Operation should be remove or split.')
  (options, _) = parser.parse_args()

  # Load the suffixes and instantiate the util object.
  with open(options.suffix_file, 'r') as f:
    suffix_list = []
    for line in f:
      suffix_list.append(line.strip().decode('utf8'))

  LemmatiseStdInAndWriteToStdOut(suffix_list, options.operation)


def LemmatiseStdInAndWriteToStdOut(suffix_list, operation):
  """This is the main function that also can do multithreading."""
  method_map = {
      'remove': 'RemoveSuffixFrom',
      'split': 'SplitSuffixFrom',
      'RemoveSuffixFrom': 'RemoveSuffixFrom',
      'SplitSuffixFrom': 'SplitSuffixFrom',
  }

  number_of_cores = multiprocessing.cpu_count()
  # Instatiate the suffix util object and get the method_name to call.
  korean_suffix_util = KoreanSuffixUtil(suffix_list)
  method_name = method_map[operation]
  # If there are only one core, we process the input line-by-line
  if True: # number_of_cores == 1:
    for line in _STDIN:
      _STDOUT.write(Lemmatise(line, korean_suffix_util, method_name)+'\n')
  else:
    # For multiple cores we read in all the data and then split it among cores.
    input_array = []
    for line in _STDIN:
      input_array.append(line.rstrip())

    batches = []
    for i in xrange(number_of_cores):
      start_range = len(input_array)*i/number_of_cores
      end_range = len(input_array)*(i+1)/number_of_cores
      batches.append(
        input_array[start_range:end_range])

    # Clear the input data to free memory.
    del input_array

    queues = []
    jobs = []
    for i in xrange(number_of_cores):
      queues.append(multiprocessing.Queue())
      jobs.append(
          multiprocessing.Process(
              target=QueueLemmatise,
              args=(queues[-1],batches[i],korean_suffix_util,method_name,)))
      jobs[-1].start()

    lemmatise_array = []
    for i in xrange(number_of_cores):
      lemmatise_array.append(queues[i].get())

    for i in xrange(number_of_cores):
      jobs[i].join()


def Lemmatise(line, suffix_util, method_name):
  """Lemmatise a line."""
  words = line.split(' ')
  words = [
    getattr(suffix_util, method_name)(w) for w in words]

  return ' '.join(words)


def QueueLemmatise(q, batch, suffix_util, method_name):
  """Wrapper for multiprocessing."""
  for line in batch:
    q.put(Lemmatise(line, suffix_util, method_name))


class KoreanSuffixUtil(object):
  """Class with suffix utils.

  We take NFC or compatiblity Hangeul and convert it to NFD before doing
  anything and then return NFC results back.
  """

  def __init__(self, suffix_list):
    # Remove duplicates and convert NFC and compatibility to NFD coda.
    # WARN: This class uses NFD internally, be careful!
    suffix_list = list(set(suffix_list))
    suffix_list = [
        unicodedata.normalize('NFD', suffix) for suffix in suffix_list]
    suffix_list = [
        hangeul_unicode_lib.HangeulCompatibilityToNFDCoda(s)
            for s in suffix_list]

    # Create a string such as 을|를|은|이|가
    suffix_or = '|'.join(suffix_list)
    hangeul_nfd_char = r'\u1100-\u11FF'
    regex = r'((?:%s)[^%s]*)$' % (suffix_or, hangeul_nfd_char)

    # Compile the regex object for performance.
    self.__suffix_regex = re.compile(regex)

  def RemoveSuffixFrom(self, word):
    """Remove suffix from the word."""
    word_nfd = unicodedata.normalize('NFD', word.strip())
    word_nfd = self.__suffix_regex.sub('', word_nfd)
    return unicodedata.normalize('NFC', word_nfd)

  def SplitSuffixFrom(self, word):
    """Split suffix from the word."""
    word_nfd = unicodedata.normalize('NFD', word.strip())
    # When spliting, re will produce an empty string at the end of the array.
    # This is because, it splits the string and on the right side the string is
    # empty. We discard this empty string. If there is no matching, then re will
    # return an empty array, in this case we leave the word as it is.
    word_array = self.__suffix_regex.split(word_nfd)[:-1]
    if word_array:
      word_nfd = ' '.join(self.__suffix_regex.split(word_nfd)[:-1])
    return unicodedata.normalize('NFC', word_nfd)


if __name__ == '__main__':
  main()
