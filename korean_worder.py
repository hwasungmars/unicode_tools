#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Remove Korean suffix for a given word."""

__author__ = 'Hwasung Lee'


import argparse
import codecs
import sys
import unicodedata

from hangeul_unicode_lib import DoesNFCWordHaveCoda
from hangeul_unicode_lib import NormalizeToCodaAndNFD


_STDOUT = codecs.getwriter('utf8')(sys.stdout)


def CombineNFCsOrReturnFirst(first, second):
  """Combine NFC words and if cannot return the first word."""
  first = first.strip()
  second = second.strip()
  if DoesNFCWordHaveCoda(first):
    return first
  else:
    combined_nfd = NormalizeToCodaAndNFD(first) + NormalizeToCodaAndNFD(second)
    return unicodedata.normalize('NFC', combined_nfd)


def main():
  """Main function for the suffix util."""
  parser = argparse.ArgumentParser(
      description='Stem Korean words from a suffix list.')
  parser.add_argument('--stem_file',
      dest='stem_file',
      help='Stem file to load.')
  parser.add_argument('--suffix_file',
      dest='suffix_file',
      help='Suffix file to load.')
  args = parser.parse_args()

  # Load the suffixes, compile the regex, and normalize the stdin to NFD.
  with codecs.open(args.stem_file, 'r', 'utf8') as f:
    stems = set(map(unicode.strip, f))
  with codecs.open(args.suffix_file, 'r', 'utf8') as f:
    suffixes = set(map(unicode.strip, f))

  # For loop for performance.
  for x in stems:
    combined = set([CombineNFCsOrReturnFirst(x, y) for y in suffixes])
    map(lambda x: _STDOUT.write(x+'\n'), combined)


if __name__ == '__main__':
  main()
