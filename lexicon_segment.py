#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Given a segmented lexicon, produce a linearized lexicon.

For example, suppose we have a lexicon with the the following segmented line.
    now a days 1020
This script will convert this into
    now 1020
    a 1020
    days 1020
"""

__author__ = 'Hwasung Lee'


import sys


def main():
  """main function which runs when the script is called standalone."""
  for line in sys.stdin:
    l = line.split()
    if len(l) > 2:
      map(lambda x: sys.stdout.write('%s %s\n' % (x, l[-1])), l[:-1])
    else:
      sys.stdout.write(line)


if __name__ == '__main__':
  main()

