#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Normalize Hangeul.

Normalize whitespaces and repeated characters. For example,
  안녕이라고   말했다. -> 안녕이라고 말했다.
  ㅋㅋㅋㅋㅋㅋ -> ㅋㅋㅋ
  안녕ㅋㅋㅋㅋㅋ -> 안녕 ㅋㅋ
"""

__author__ = 'Hwasung Lee'


import sys
import re


def LimitDuplication(line, nb_duplications=2):
  """Uses regex to limit the number of duplications."""
  regex = r'(.)\1{%s,}' % nb_duplications
  subexp = r'\1' * nb_duplications
  return re.sub(r'(\.(\s*\.)+|…|···)', r'.',
                re.sub(regex, subexp, line))


def main():
  """main function which runs when the script is called standalone."""
  for line in sys.stdin:
    sys.stdout.write(LimitDuplication(line))


if __name__ == '__main__':
  main()

