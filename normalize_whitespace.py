#!/usr/bin/env python

"""Normalizes whitespaces by reducing multiple whitespaces/tabs to one."""

__author__ = 'Hwasung Lee'


import codecs
import re
import sys


_STDIN = codecs.getreader('utf8')(sys.stdin)
_STDOUT = codecs.getwriter('utf8')(sys.stdout)


def main():
  """main function which runs when the script is called standalone."""
  whitespace_re = re.compile(ur'\s+')
  for line in _STDIN:
    line = line.strip()
    line = whitespace_re.sub(u' ', line)

    if line:
      _STDOUT.write(line + '\n')


if __name__ == '__main__':
  main()

