#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Given a sorted lexicon find the top frequent words.

By default this finds 20% of the top words.
"""

__author__ = 'Hwasung Lee'


import argparse
from collections import Counter
import sys


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(description='Get top r ratio.')
  parser.add_argument('-r', '--ratio',
      dest='ratio',
      type=float,
      default=0.2,
      help='Top ratio of word coverage.')
  args = parser.parse_args()

  lexicon = Counter()
  for line in sys.stdin:
    l = line.strip().split()
    lexicon[' '.join(l[:-1])] += int(l[-1])

  total = sum(lexicon.values())
  sum_so_far = 0
  for k, v in lexicon.most_common():
    sum_so_far += v
    if sum_so_far < args.ratio*total:
      sys.stdout.write('%s %s\n' % (k, v))
    else:
      break


if __name__ == '__main__':
  main()

