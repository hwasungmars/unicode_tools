#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Grep tool using Python."""

__author__ = 'Hwasung Lee'


import argparse
import re
import sys


def TrueFalseWrapper(truthy):
  """Converts truthy to explicit True/False.

  This function is useful when comparing re's truthy results such as None to
  explicit True/False.
  """
  if truthy:
    return True
  else:
    return False


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(description='Parse arguments for grepping.')
  parser.add_argument('-v', '--inverse',
      action='store_true',
      default=False,
      dest='inverse',
      help='Inverse the logic.')
  parser.add_argument('-r', '--regex',
      required=True,
      dest='regex',
      help='Regex to match.')
  args = parser.parse_args()

  compiled_re = re.compile(r'%s' % args.regex)
  for line in sys.stdin:
    if TrueFalseWrapper(compiled_re.search(line)) != args.inverse:
      sys.stdout.write(line)


if __name__ == '__main__':
  main()

