#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Unicode sort using Python.

We use sample sort with Python default sort to sort the elements.
"""

__author__ = 'Hwasung Lee'


import argparse
import sys


def main():
  """Read in data from _STDIN and write out to _STDOUT."""
  parser = argparse.ArgumentParser(
      description='Python sort which deals with Unicode in a graceful way.')
  parser.add_argument('-u', '--unique',
      dest='unique',
      action='store_true',
      default=False,
      help='Unique the elements.')
  args = parser.parse_args()

  if args.unique:
    result = sorted(set(map(str.strip, sys.stdin)))
  else:
    result = sorted(map(str.strip, sys.stdin))

  for line in result:
    sys.stdout.write('%s\n' % line)


if __name__ == '__main__':
  main()
