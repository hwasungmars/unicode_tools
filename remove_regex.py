#!/usr/bin/env python3.2
# -*- coding: utf8 -*-

"""Remove a given regex pattern from stdin."""

__author__ = 'Hwasung Lee'


import argparse
import re
import sys


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(
      description='Gets the regex.')
  parser.add_argument('-r', '--regex',
      dest='regex',
      help='Regex to remove from stdin.')
  args = parser.parse_args()

  sys.stderr.write('Compiling regex.\n')
  remove_re = re.compile(args.regex)

  sys.stderr.write('Processing data.\n')
  for line in sys.stdin:
    sys.stdout.write(remove_re.sub('', line))


if __name__ == '__main__':
  main()

