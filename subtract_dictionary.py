#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Takes input from stdin and return words that are not in the dictionary."""

__author__ = 'Hwasung Lee'


import argparse
import sys


def TrueFalseWrapper(truthy):
  """Converts truthy to explicit True/False.

  This function is useful when comparing re's truthy results such as None to
  explicit True/False.
  """
  if truthy:
    return True
  else:
    return False


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(
      description='Show words that are not in the dictionary.')
  parser.add_argument('-d', '--dictionary',
      dest='dictionary',
      required=True,
      help='Dictionary file.')
  parser.add_argument('-v', '--inverse',
      dest='inverse',
      action='store_true',
      default=False,
      help='Inverse the logic, i.e. find intersection.')
  args = parser.parse_args()

  with open(args.dictionary, 'r') as f:
    dictionary = set(
        map(lambda x: x.strip().split()[0], f))

  for line in map(str.strip, sys.stdin):
    word = line.split()[0]
    if TrueFalseWrapper(word in dictionary) == args.inverse:
      sys.stdout.write('%s\n' % line)


if __name__ == '__main__':
  main()

