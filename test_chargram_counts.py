"""Describe what this package does."""

__author__ = 'Hwasung Lee'


import unittest
import chargram_counts


class TestMrChargramCounts(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testChargramPerWord(self):
    """Test Chargram per word."""
    word = 'hello'
    expected = [('h', 1), ('e', 1), ('l', 2), ('o', 1), ('he', 1), ('el', 1),
        ('ll', 1), ('lo', 1), ('hel', 1), ('ell', 1), ('llo', 1), ('hell', 1),
        ('ello', 1), ('hello', 1)]
    result = chargram_counts.ChargramPerWord(word)
    self.assertEqual(set(expected), set(result))


if __name__ == '__main__':
  unittest.main()

