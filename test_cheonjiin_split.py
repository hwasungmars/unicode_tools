#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests Cheonjiin decompose."""

__author__ = 'Hwasung Lee'


import unittest
import cheonjiin_split


class TestCheonjiinSplit(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testCheonjiinDecompose(self):
    """Tests CheonjiinDecompose."""
    line = 'abcㅏㅐㅑㅒ'
    expected = 'abcㅣ\u318Dㅣ\u318Dㅣㅣ\u318D\u318Dㅣ\u318D\u318Dㅣ'
    result = cheonjiin_split.CheonjiinDecompose(line)
    self.assertEqual(expected, result)

    line = 'ㅓㅔㅕㅖ'
    expected = '\u318Dㅣ\u318Dㅣㅣ\u318D\u318Dㅣ\u318D\u318Dㅣㅣ'
    result = cheonjiin_split.CheonjiinDecompose(line)
    self.assertEqual(expected, result)

    line = 'ㅗ ㅘ ㅙ ㅚ ㅛ'
    expected = ('\u318Dㅡ \u318Dㅡㅣ\u318D \u318Dㅡㅣ\u318Dㅣ \u318Dㅡㅣ '
        '\u318D\u318Dㅡ')
    result = cheonjiin_split.CheonjiinDecompose(line)
    self.assertEqual(expected, result)

    line = 'ㅜ ㅝ ㅞ ㅟ ㅠ'
    expected = ('ㅡ\u318D ㅡ\u318D\u318Dㅣ ㅡ\u318D\u318Dㅣㅣ ㅡ\u318Dㅣ '
        'ㅡ\u318D\u318D')
    result = cheonjiin_split.CheonjiinDecompose(line)
    self.assertEqual(expected, result)

    line = 'ㅡ ㅢ ㅣ'
    expected = ('ㅡ ㅡㅣ ㅣ')
    result = cheonjiin_split.CheonjiinDecompose(line)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

