#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Test hangeul extract package."""

__author__ = 'Hwasung Lee'


import unittest

import hangeul_extract

class TestHangeulFilter(unittest.TestCase):
  """Test class that inherits the methods."""

  def testPassNFxAndCompatibility(self):
    """Test whether filter function works."""
    hangeul_line = '한글(hangeul)을 쓰자ㅋㅋ'
    expected = '한글을 쓰자ㅋㅋ'

    hangeul_utils = hangeul_extract.HangeulUtils()
    result = hangeul_utils.FilterNFxAndCompatibility(hangeul_line)
    self.assertEqual(expected, result)

  def testPassNFC(self):
    """Test whether filter function works."""
    hangeul_line = '한글(hangeul)을 쓰자ㅋㅋ'
    expected = '한글을 쓰자'

    hangeul_utils = hangeul_extract.HangeulUtils()
    result = hangeul_utils.FilterNFC(hangeul_line)
    self.assertEqual(expected, result)



if __name__ == '__main__':
  unittest.main()
