#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests for Hangeul sentence extraction."""

__author__ = 'Hwasung Lee'


import unittest
import hangeul_extract_sentence


class TestHangeulExtractSentence(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testHangeulFraction(self):
    """Tests whether we get the correct fraction."""
    line = '하하하ㅋㅋㅋ'
    expected = 1
    result = hangeul_extract_sentence.HangeulFraction(line)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

