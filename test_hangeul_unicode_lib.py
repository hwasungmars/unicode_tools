#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Test Hangeul Unicode converter."""

__author__ = 'Hwasung Lee'


import unicodedata
import unittest

import hangeul_unicode_lib


class TestHangeulUnicodeConverter(unittest.TestCase):
  """Testing Hangeul Unicode converter."""

  def testIsCharHangeulNFC(self):
    """Tests whether we correctly identify NFC Hangeul."""
    char = '한'
    result = hangeul_unicode_lib.IsCharHangeulNFC(char)
    self.assertTrue(result)

    char = 'ㅎ'
    result = hangeul_unicode_lib.IsCharHangeulNFC(char)
    self.assertFalse(result)

  def testIsCharHangeul(self):
    """Tests whether we correctly identify Hangeul."""
    char = '한'
    result = hangeul_unicode_lib.IsCharHangeul(char)
    self.assertTrue(result)

    char = 'ㅎ'
    result = hangeul_unicode_lib.IsCharHangeul(char)
    self.assertTrue(result)


  def testDoesNFCWordHaveCoda(self):
    """Tests whether NFC word has coda."""
    word = '한글'
    result = hangeul_unicode_lib.DoesNFCWordHaveCoda(word)
    self.assertTrue(result)

    word = '하나'
    result = hangeul_unicode_lib.DoesNFCWordHaveCoda(word)
    self.assertFalse(result)

  def testHangeulCompatibilityToNFDCoda(self):
    """Tests Compatibilitly to NFD coda conversion."""
    line = '한글 ㄴ'
    expected = '한글 \u11AB'
    result = hangeul_unicode_lib.HangeulCompatibilityToNFDCoda(line)
    self.assertEqual(expected, result)

  def testNormalizeToCodaAndNFD(self):
    """Test whether normalize to coda and NFD works."""
    mixed_word = '하ㄴ그ㄹ'
    expected = '한글'
    result = unicodedata.normalize(
        'NFC', hangeul_unicode_lib.NormalizeToCodaAndNFD(mixed_word))
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

