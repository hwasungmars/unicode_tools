#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Test the Korean suffix remover."""

__author__ = 'Hwasung Lee'


import unittest

import korean_suffix_util


class TestKoreanSuffixUtil(unittest.TestCase):
  """Testing Korean suffix util."""

  def testRemoveSuffixFrom(self):
    """Test suffix remover."""
    suffix_list = ['을', '를', 'ㄴ']
    korean_suffix_util_obj = korean_suffix_util.KoreanSuffixUtil(suffix_list)

    original_word = '한글을'
    expected = '한글'
    result = korean_suffix_util_obj.RemoveSuffixFrom(original_word)
    self.assertEqual(expected, result)

    original_word = '아름다운'
    expected = '아름다우'
    result = korean_suffix_util_obj.RemoveSuffixFrom(original_word)
    self.assertEqual(expected, result)

  def testSplitSuffixFrom(self):
    """Test split suffix."""
    suffix_list = ['을', '를', 'ㄴ들']
    korean_suffix_util_obj = korean_suffix_util.KoreanSuffixUtil(suffix_list)

    original_word = '한글을'
    expected = '한글 을'
    result = korean_suffix_util_obj.SplitSuffixFrom(original_word)
    self.assertEqual(expected, result)

    original_word = '그렇게한들'
    expected = '그렇게하 \u11AB들'
    result = korean_suffix_util_obj.SplitSuffixFrom(original_word)
    self.assertEqual(expected, result)

    original_word = '한글을,'
    expected = '한글 을,'
    result = korean_suffix_util_obj.SplitSuffixFrom(original_word)
    self.assertEqual(expected, result)

  def testLemmatise(self):
    """Test lemmatise which works on lines."""
    suffix_list = ['을', '를', 'ㄴ들']
    korean_suffix_util_obj = korean_suffix_util.KoreanSuffixUtil(suffix_list)
    line = '한글을, 사용하자.'
    expected = '한글 을, 사용하자.'
    result = korean_suffix_util.Lemmatise(
        line, korean_suffix_util_obj, 'SplitSuffixFrom')
    self.assertEqual(expected, result)




if __name__ == '__main__':
  unittest.main()

