#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests Korean worder."""

__author__ = 'Hwasung Lee'


import unittest

import korean_worder


class TestKoreanWorder(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testCombineNFDsOrReturnFirst(self):
    """Combine two NFD expressions if not possible return the first."""
    first = '새'
    second = '로운'
    expected = '새로운'
    result = korean_worder.CombineNFCsOrReturnFirst(first, second)
    self.assertEqual(expected, result)

    first = '새로운가'
    second = 'ㅂ다'
    expected = '새로운갑다'
    result = korean_worder.CombineNFCsOrReturnFirst(first, second)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

