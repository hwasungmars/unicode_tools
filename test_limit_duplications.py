#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Describe what this package does."""

__author__ = 'Hwasung Lee'


import unittest
import limit_duplications


class TestHangeulLimitDups(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testLimitDuplication(self):
    """Test segmenting a line."""
    line = '깜찍 박사님ㅋㅋㅋㅋ'

    expected = '깜찍 박사님ㅋㅋㅋ'
    result = limit_duplications.LimitDuplication(line, 3)
    self.assertEqual(expected, result)

    expected = '깜찍 박사님ㅋㅋ'
    result = limit_duplications.LimitDuplication(line)
    self.assertEqual(expected, result)

    expected = '깜찍 박사님ㅋㅋ'
    result = limit_duplications.LimitDuplication(line, 2)
    self.assertEqual(expected, result)

    line = '이상한 . . . 음킁… 박사님...'
    expected = '이상한 . 음킁. 박사님.'
    result = limit_duplications.LimitDuplication(line, 2)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

