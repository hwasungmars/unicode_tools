#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Test dictionary based greedy tokenizer."""

__author__ = 'Hwasung Lee'


import unittest
import re
import tokenizer_greedy


class TestTokenizer(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testTestTokenizeLine(self):
    """Tokenize a line."""
    segment_re = re.compile(r'([abc])(.*)')
    line = 'abc'
    expected = 'a b c'
    result = tokenizer_greedy.TokenizeLine(line, segment_re)
    self.assertEqual(expected, result)

    line = ''
    expected = ''
    result = tokenizer_greedy.TokenizeLine(line, segment_re)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

