#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests unigram based tokenizer."""

__author__ = 'Hwasung Lee'


import math
import tokenizer_unigram
import unittest


class TestTokenizerUnigram(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testSegmentUsingRegexes(self):
    """Tests whether the segments block correctly."""
    regexes = tokenizer_unigram.CompileRegexes()
    line = '한겨레21'
    expected = '한겨레 21'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '가-힣a'
    expected = '가 - 힣 a'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '2013년, 6%대로 떨어졌다'
    expected = '2013년 , 6%대로 떨어졌다'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = 'ㅋㅋ박사님ㅋㅋㅋ'
    expected = 'ㅋㅋ 박사님 ㅋㅋㅋ'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '^^/깜찍^^*'
    expected = '^^/ 깜찍 ^^*'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '3.14랑.오랑. google에서'
    expected = '3.14랑 . 오랑 . google에서'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '"깜찍."이라고 "갑짜기"말했다.'
    expected = '" 깜찍 . "이라고 " 갑짜기 "말했다 .'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '깜찍 박사님 왈“성냥팀”이라'
    expected = '깜찍 박사님 왈 “ 성냥팀 ”이라'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

    line = '이것이 @TAG의 예이다.전화@LOWFREQ@ALPHANUM.'
    expected = '이것이 @TAG의 예이다 . 전화 @LOWFREQ @ALPHANUM .'
    result = tokenizer_unigram.SegmentUsingRegexes(line, regexes)
    self.assertEqual(expected, result)

  def testPwords(self):
    """Tests whether Pwords find the log probability correctly."""
    logprob = {'a': 0, 'b': -1}
    words = ['a', 'b']
    expected = -1
    result = tokenizer_unigram.Pwords(words, logprob)
    self.assertEqual(expected, result)

    logprob = {'a': 0, 'b': -1}
    words = ['a', 'b', 'c']
    expected = -1 - 137*math.log(2)
    result = tokenizer_unigram.Pwords(words, logprob)
    self.assertEqual(expected, result)

  def testSegmentWord(self):
    """Tests whether segment returns the best possible segmentation."""
    logprob = {'a': 0, 'ab': -1}
    text = 'ab'
    expected = ['ab']
    result = tokenizer_unigram.SegmentWord(text, logprob)
    self.assertEqual(expected, result)

    logprob = {'a': 0, 'ab': -1}
    text = '6ab'
    expected = ['6', 'ab']
    result = tokenizer_unigram.SegmentWord(text, logprob)
    self.assertEqual(expected, result)

    logprob = {'b': -1}
    text = '6ab'
    expected = ['6a', 'b']
    result = tokenizer_unigram.SegmentWord(text, logprob)
    self.assertEqual(expected, result)

    logprob = {'a': 0, 'ab': -1}
    text = '6'
    expected = ['6']
    result = tokenizer_unigram.SegmentWord(text, logprob)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

