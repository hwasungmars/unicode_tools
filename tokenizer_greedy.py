#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Dictionary based tokenizer.

It takes in a dictionary and does greedy matching at the beginning of the string
and tokenizes the given text.
"""

__author__ = 'Hwasung Lee'


import argparse
import codecs
import re
import sys


_STDIN = codecs.getreader('utf8')(sys.stdin)
_STDOUT = codecs.getwriter('utf8')(sys.stdout)
_STDERR = codecs.getwriter('utf8')(sys.stderr)


def TokenizeLine(line, segment_re):
  """Tokenizes a line by regex or whitespace."""
  line = line.strip()
  if len(line) == 0:
    return ''

  m = segment_re.match(line)
  if m:
    if len(m.group(2)) == 0:
      return m.group(1)
    else:
      return m.group(1) + ' ' + TokenizeLine(m.group(2), segment_re)
  else:
    l = line.split()
    if len(l) > 1:
      return l[0] + ' ' + TokenizeLine(' '.join(l[1:]), segment_re)
    else:
      return l[0]


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(
      description='Tokenize context with a dictionary.')
  parser.add_argument('-d', '--dictionary',
      dest='dictionary',
      help='Dictionary file to load.')
  args = parser.parse_args()

  _STDERR.write('Reading the dictionary in.\n')
  with codecs.open(args.dictionary, 'r', 'utf8') as f:
    words = set(map(unicode.strip, f))
  _STDERR.write('Compiling the regex.\n')
  segment_re = re.compile(r'(%s)(.*)' % '|'.join(words))

  _STDERR.write('Tokenizing.\n')
  for line in _STDIN:
    _STDOUT.write(TokenizeLine(line, segment_re) + '\n')


if __name__ == '__main__':
  main()

