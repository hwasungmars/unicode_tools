#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Segment a word by unigram probability."""

__author__ = 'Hwasung Lee'


import argparse
from functools import partial
# pylint: disable-msg=W0622
from functools import reduce
# pylint: enable-msg=W0622
import math
import multiprocessing
import re
import sys


def CompileRegexes():
  """Compile a family of regex to use to segment text."""
  compiled_regexes = []
  # Ending quotation marks are tricky. We have two base cases we would like to
  # cover. Consider the following sentence:
  #   "박사님을 구박한다"라고
  #   1. If " is after a beginning of sentence (BOS) or whitespace we should
  #      segement:
  #       " 박사님을 구박한다"라고
  #   2. If " is after a non-bos or non-whitespace, we should segment it off the
  #      non-bos or non-whitespace but keep it with the suffix:
  #       " 박사님을 구박한다 "라고
  # WARN: Fundamentally, these two operations are not commutative.
  end_quotes = r'>》〉」』】’”\'"'
  compiled_regexes.append(re.compile(r'((?:\s|^)[%s])([^%s\s])' %
    (end_quotes, end_quotes)))
  compiled_regexes.append(re.compile(r'([^%s\s])([%s])' %
    (end_quotes, end_quotes)))

  # Segment tags such as @LOWFREQ:
  compiled_regexes.append(re.compile(r'([^\s])(@[A-Z])'))

  special = r'▲▼■ⓒ~·…,/☆〜※?;\\:\|\[\]{}!#$^&*\(\)_+\-=↑<《〈「『【‘“`'
  compiled_regexes.append(re.compile(r'([%s])([^%s\s])' % (special, special)))
  compiled_regexes.append(re.compile(r'([^%s\s])([%s])' % (special, special)))

  # If period is not followed by a digit, a whitespace, nor a period, we
  # consider it as a end of sentence character.
  compiled_regexes.append(re.compile(r'(\.)([^\d\s\.]+)'))
  compiled_regexes.append(re.compile(r'([^\d\s\.]+)(\.)'))

  # Segment camel casing.
  compiled_regexes.append(re.compile(r'([a-z]+)([A-Z]+)'))

  # If Hangeul is followed by non-Hangeul or non-whitespace we segment.
  compiled_regexes.append(re.compile(r'([가-힣]+)([^가-힣\s]+)'))

  # Segment Jamo from Hangeul.
  compiled_regexes.append(re.compile(r'([가-힣]+)([ㄱ-ㅣ]+)'))
  compiled_regexes.append(re.compile(r'([ㄱ-ㅣ]+)([가-힣]+)'))

  return compiled_regexes


def SegmentUsingRegexes(text, regexes=None):
  """Segment a text according to given regexes.

  If no regexes is given it will not segment and return the text.
  """
  if regexes:
    for r in regexes:
      text = r.sub(r'\1 \2', text)
  return text


def Pwords(words, logprob):
  """Probability of a sequence of words."""
  # -137 to penalize unknown long words and log to group unknown word together
  return sum(logprob[w] if w in logprob else
             -137*math.log(len(w)+1) for w in words)


def SegmentWord(text, logprob):
  """Best segmentation of text into words, by probability."""
  def Splits(chars):
    """All ways to split chars into a first word and remainder."""
    return [(chars[:i], chars[i:]) for i in range(1, 1+len(chars))]

  # The longest text we will attempt to segment. If it is longer than this we
  # give up and return the text without segmenting it.
  _longest = 13

  if text == '':
    return []
  elif len(text) > _longest:
    return [text]
  else:
    candidates = [[first] + SegmentWord(rest, logprob)
        for first, rest in Splits(text)]
    return max(candidates, key=lambda x: Pwords(x, logprob))


def SegmentLine(line, lprob):
  """Tokenizes a line by finding the best logprob segmentation."""
  # Segment each words.
  word_segments = map(lambda x: SegmentWord(x, lprob), line.strip().split())
  # Flatten each wordlist to a single list on line level and join.
  return ' '.join(reduce(lambda x, y: x+y, word_segments, []))


def CreateLogProbDict(lexicon_path):
  """Takes a lexicon_path as input and return a log probability dictionary."""
  f = open(lexicon_path, 'r')
  sys.stderr.write('Loading lexicon.\n')
  lexicon = [(x[0], int(x[1])) for x in map(lambda x: x.strip().split(), f)]
  f.close()

  sys.stderr.write('Creating logprob dictionary.\n')
  total = reduce(lambda t, c: t+c[1], lexicon, 0)
  return dict(map(lambda x: (x[0], math.log(x[1]/total)), lexicon))


def TokenizeToStdout(line, lprob):
  """End-to-end tokenization that can be pickled."""
  regexes = CompileRegexes()
  tokenizing = [SegmentUsingRegexes(x, regexes) for x in line.strip().split()]
  if lprob:
    tokenizing = [SegmentLine(x, lprob) for x in tokenizing]

  sys.stdout.write('%s\n' % ' '.join(tokenizing))


def main():
  """main function which runs when the script is called standalone."""
  parser = argparse.ArgumentParser(
      description='Unigram based segmentor.')
  parser.add_argument('-l', '--lexicon',
      dest='lexicon',
      default=None,
      help='Lexicon file to load.')
  args = parser.parse_args()

  sys.stderr.write('Setting up workers.\n')
  workers = multiprocessing.Pool(multiprocessing.cpu_count())
  logprob = CreateLogProbDict(args.lexicon) if args.lexicon else None
  MapTokenizeToStdout = partial(TokenizeToStdout, lprob=logprob)

  sys.stderr.write('Tokenizing.\n')
  workers.map(MapTokenizeToStdout, sys.stdin)


if __name__ == '__main__':
  main()

